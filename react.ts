// Formas para exportar Funciones
// https://stackoverflow.com/questions/33178843/es6-export-default-with-multiple-functions-referring-to-each-other/33179024#33179024

// los siguientes comandos se utilizarán para crear un nuevo proyecto
npx create-react-app my-app
cd my-app
npm start

// para instalar react y react DOM por aparte creo, ocupamos el siguiente comando:
// para la sintaxis de jsx(sintaxis para construir elementos html dentro de javascript) intalar el packete en ATOM :  "languaje-babel"

npm install react react-dom --save


// Para validar el tipado de las propiedades(decirle que tipo de dato es: texto, img, numero, function etc..) en react, es decir al hacer bind entre componentes o extraer info para mostrar en determinado componente puede suceder el caso que esta info llegue mal, y para no estropear la app y hacer un manejo correcto de esto instalaremos:
npm install prop-types --save

// ejemplo de uso

  {
    ClassName.propTypes = {
      image:PropTypes.string, //esto nos valida un string como el tipo de dato de la propiedad image que debe de llegar al componente, si no llega nos lanzara un warning
      title:PropTypes.number, //podemos validar otros tipos de datos como arreglo(.array), funciones(.func), numeros(.number), objetos(.object) etc
      title:PropTypes.number.isRequired , //Para que una propiedad sea obligatoria
      type:PropTypes.oneOf(["uno", "dos"]) //con la función uno de(oneOf) podemos validar para que solo se acepten datos especificos, en este ejemplo la propiedad solo puede recibir como datos "uno" o "dos", si  recibe un dato diferente lanzará una alerta
    }

    // Default props por si la propiedad esta indefinida coloque un valor por defecto
    Media.defaultProps = {
      title:"Mensaje por default"
    }
  }
  // Documentación completa en : https://reactjs.org/docs/typechecking-with-proptypes.html










  <<EVENT HANDLERS>>//----------------------------------------------------------------------------------------------------------------------------------------------------

  // https://reactjs.org/docs/handling-events.html#passing-arguments-to-event-handlers
  // Forma para heredar el contexto de la clase a una función por medio de un evento onClick

  class LoggingButton extends React.Component {
  handleClick() {
    console.log('this is:', this);
  }

  render() {
    // This syntax ensures `this` is bound within handleClick
    return (
      <button onClick={(e) => this.handleClick()}>
        Click me
      </button>
    );
  }
}


// con javascript moderno:

class LoggingButton extends React.Component {

  handleClick = (event) => {
    this.setState({
      author: 'ricardo selis',
    })
    console.log('El author de este video es: ', this.state.author);
  }

  <div className = "medias" onClick={this.handleClick}>
  </div>

}






<<ESTADOS>>//--------------------------------------------------------------------------------------------------------------------------------------

// Las PROPIEDADES (props) son INMUTABLES, es decir; estas no pueden cambiar. En el caso que quisiéramos que algo cambie en nuestro componente, hay que manejarlo con el estado de nuestros componentes, el cual permite tener contenido dinámico.
// Para inicializar el estado tenemos que hacer uso de nuestro metodo constructor:
constructor(props){
    super(props);
    this.state = {
        author: props.author
    }
}

// Y en nuestros elementos vamos a utilizar state en lugar de props:

<p className="Media-author">{this.state.author}</p>

// Para cambiar el estado tenemos un método especifico setSate() al cual le pasamos los valores que queremos modificar en nuestro estado.

handleClick() {
    this.setState({
        author: 'Emmanuel Alonso'
    })
}







<<CICLO DE VIDA DE LOS COMPONENTES>>//----------------------------------------------------------------------------------------------

class MiComponente extends Components{

	constructor(){
  // método llamado antes de que el componente sea montado (componente aun no se ve).
    // Podemos iniciar el estado
    // Enlazar eventos (bind).
    // Es el primer metodo que se llama al instanciar un componente.
	}

	componentWillMount(){
    // método llamado inmediatamente antes de que el componente se vaya a montar (componente aun no se ve).
        // Podemos hacer un setState()
        // No hacer llamados a un API o suscripción a eventos.
	}

	render(){
  //método que contiene todos los elementos a renderizar (estructura del componente).
    // Contiene JSX en el return.
    // Puedes calcular propiedades nCompleto = name + lastName.
	}

	componentDidMount(){
  //Método llamado luego de montarse el componente (el componente ya esta en la pantalla).
      // Solo se lanza una vez.
      // Enlazar (bind) de eventos.
      // Es el primer método que se llama al instanciar un componente.
      // Aqui podemos utilizar APIs (Navegador o Datos Externos).
	}

	//Actualización:

	componentWillReceiveProps(){
  // método llamado al recibir nuevas propiedades que sirve para actualizar el estado con base a las nuevas propiedades.
	}
	shouldComponentUpdate(){
  // método que condiciona si el componente se debe volver a renderizar, es utilizado para optimizar el rendimiento.
	}
	componentWillUpdate(){
  // método llamado antes de re-renderizar un componente, es utilizado para optimizar el rendimiento.
	}

	// re-render si es necesario...
	componentDidUpdate(){
    // Después de que el componente se re-renderice.
	//Método llamado luego del re-render
	}
	componentWillUnmount(){
    // cuando el componente está siendo eliminado
  // método llamado antes de que el componente sea retirado de la aplicación.
	}
	componentDidCatch(){
	// Si ocurre algún error, lo capturo desde acá:
   // método llamado cuando ocurre un error al renderizar el componente, el manejo de errores solamente ocurre en componentes hijos.
	}
}




<<SPREAD OPERATOR>> //---------------------------------------------------------------------------------------------------------------------------------------------------------
// el spread operator nos sirve para enviar un objeto en el componente padre y recibir las propiedades de ese objeto como variables independientes en el componente hijo ejemplo:


// objeto
const atributos ={
  size:35,
  color:"red"
}

// Componente PADRE
<Play {...atributos}/> //le enviamos el objeto por medio del spread component al componente HIJO llamado PLAY

// Componente HIJO

function Play(props){

  const {
    color,
    size
  } = props

  return(
      <div height={size} width={size} background={color} >
        Este es el componente hijo
      </div>
  )
}

export default PLAY;

// -------------------------------------------------------------------------------------------------------------------------------------



<<COMPOSICIÓN DE COMPONENTES>>//----------------------------------------------------------------------------------------------------------------------------------------------------
// Hay 2 tipos de componentes, los presentacionales(componente tonto/UI/como se ve) y los componentes de estado(inteligentes/como funciona/logica)

// componente tonto -- Cómo se ve
//
// Puede contener smart components u otros componentes de UI
// Permiten composición con `[props.children]``
// No depeden del resto de la aplicación
// No especifica cómo los datos son cargados o mutados
// Recibe datos y callbacks solo con propiedades
// Rara vez tienen su propio estado
// Están escritos como componentes funcionales a menos que necesiten mejoras de performance. Sólo pueden ser Componentes funcionales o Pure Components

// Componente inteligente -- Qué hace
//
// Concetrado en el funcionamiento de la aplicación
// Contienen componentes de UI u otros containers
// No tienen estilos
// Proveen de datos a componentes de UI u otros contenedores
// Proveen de callbacks a la UI
// Normalmente tienen estado
// Llaman acciones
// Generados por higher order components

// Ejemplo de una composición de 2 componentes(ui/funcionalidad), crearemos un componente funcional para llamar iconos especificos que a su vez tendra otro componente UI que funcionará  utilizando props.children

// componente de visualización UI(tonto)

import React from 'react';

function Icon(props){

  const {
    color,
    size
  } = props


  return(
      <svg
        viewBox="0 0 32 32"
        height={size}
        width={size}
        fill={color}
        >
        {props.children}
      </svg>
  )
}

export default Icon;

// componente inteligente:


import React from 'react';
import Icon from './icon';


export function Play(props) {
  return(
    <Icon {...props} >
        <path d="M6 4l20 12-20 12z"></path> //esto es lo que recibe en {props.children}
    </Icon>
  )
}


export function Pause(props){
  return(
    <Icon {...props} >
        <path d="M4 4h10v24h-10zM18 4h10v24h-10z"></path>
    </Icon>
  )
}

export default Play;


// componente padre, que llamara el resultado de la composición de los 2 componentes anteriores

const atributos ={
  size:30,
  color:"black"
}

<Play  {...atributos}/>
// atributos llega como props en el componente inteligente, que a su vez se lo envía al componente Icon que es el que utiliza las propiedades heredadas
