import React, { Component } from 'react';
import './media.css';
import data from '../json/api.json';
import Media from './media';

class Playlist extends Component {


  render() {

    var playlist;

    switch(this.props.categoria) {
    case 'bestWeek':
         playlist = data.categories[0].playlist
        break;
    case 'bestFocus':
         playlist = data.categories[1].playlist
        break;
    case 'forDevs':
        playlist = data.categories[2].playlist
        break;
    default:
        playlist = data.categories[0].playlist
}

    return (
      <div className="App">

        {
          playlist.map((item)=>{
            return <Media {...item} key={item.id} />
          })
        }

      </div>
    );
  }
}

export default Playlist;
