import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './media.css';

class Media extends Component {

// Aqui definiré las propiedades dinamicas que serán renderizadas
state = {
    author: this.props.author
}

  handleClick = (event) => {
    this.setState({
      author: 'ricardo selis',
    })
    console.log('El author de este video es: ', this.state.author);
  }

  render(){
    const { title, type, cover } = this.props;
    const { author } = this.state;
    this.author = author;

    return(
      <div className = "medias" onClick={this.handleClick}>
          <div className = "bordeado">
            <img src={cover} alt="cabrona" />
            <p className = "fuente title" ><b>{title}</b></p>
            <p className = "fuente">{author}</p>
            <p className = "fuente types">{type}</p>
          </div>
      </div>
    )
  }
}


Media.propTypes = {
  cover:PropTypes.string,
  title:PropTypes.string,
  author:PropTypes.string,
  type:PropTypes.oneOf(['Media', 'video'])
}

Media.defaultProps = {
  title:"Mensaje por Default"
}

export default Media;
