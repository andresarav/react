// import React, { Component } from 'react';
import React from 'react';
import './media.css';
import Media from './media';

// class Playlist2 extends Component{
//
//   render() {
//
//     return (
//      <div className="App">
//        {
//          this.props.data.map((item)=>{
//            return <Media {...item} key={item.id} />
//          })
//        }
//      </div>
//     );
//   }
//
// }


const Playlist2 = (props) => {
console.log('estas son las propiedades heredadas:', props)
  return(
    <div className="App">
      {
        props.data.map((item)=>{
          return <Media {...item} key={item.id} />
        })
      }
    </div>
  )
}

export default Playlist2;
