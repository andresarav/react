import React from 'react';
import './index.css'
import { render } from 'react-dom';
import componentes from './App';
import registerServiceWorker from './registerServiceWorker';

render(<componentes.App />, document.getElementById('root'));
registerServiceWorker();
