import './App.css';
import React, { Component } from 'react';
import Playlist from './components/playlist/playlist';
import Playlist2 from './components/playlist/playlist2'; //Ejemplo aplicado con un componente funcional
// import {Play, Pause, Volume, FullScreen} from './components/icons/components/play';
import data from './components/json/api.json'



class App extends Component{

  render(){

    return(
      <div>

        <div className="bestWeek">
          <div className="titulos">{data.categories[0].description}</div>
            <Playlist2 data={data.categories[0].playlist}/>
        </div>

        <div className="bestWeek">
          <div className="titulos">{data.categories[1].description}</div>
            <Playlist2 data={data.categories[1].playlist}/>
        </div>

        <div className="bestWeek">
          <div className="titulos">Lo mejor de la semana</div>
            <Playlist categoria="bestWeek" />
        </div>

        <div className="bestWeek">
          <div className="titulos">Para concentrarse</div>
            <Playlist categoria="bestFocus" />
        </div>

        <div className="bestWeek">
          <div className="titulos">Para programar</div>
            <Playlist categoria="forDevs" />
        </div>
      </div>
    )
  }

}


const componentes = {
  Playlist,
  App
}

// export default App;
export default componentes;
// render(<App data={data} />, app);
